import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import LanguageDetector from "i18next-browser-languagedetector";
 
import { TRANSLATIONS_GR } from "./gr/translation";
import { TRANSLATIONS_EN } from "./en/translation";
import { TRANSLATIONS_PL } from "./pl/translation";
 
i18n
 .use(LanguageDetector)
 .use(initReactI18next)
 .init({
   resources: {
     en: {
       translation: TRANSLATIONS_EN
     },
     gr: {
       translation: TRANSLATIONS_GR
     }
     ,
    pl: {
       translation: TRANSLATIONS_PL
     }
   }
 });
 
 if (!sessionStorage.getItem("lang")){
  i18n.changeLanguage("pl");
  sessionStorage.setItem("lang","pl");
}
    