import React, { useEffect, useState } from "react";
import API from "../../API/API";
import "./HomeComponent.css"
import {Carousel} from "react-bootstrap";
import {useTranslation} from "react-i18next";

function HomeComponent() {

    const {t} = useTranslation();

    return(
        <div className="Home">
            <h5>{t("HOME")}</h5>
            <Carousel className="home-carousel" variant="dark">
                <Carousel.Item>
                    <div className="row align-items-center">
                        <div className="col-4">
                            <img
                                className="d-block w-100 photo"
                                src="university1.jpg"
                                alt="First slide"
                            />
                        </div>
                        <div className="col-8 py-2">
                            <p> Text text text here. Text here, here, here, here is the text. Text here, here, here,
                                here is the text. </p>
                            <p> Text text text here. Text here, here, here, here is the text. Text here, here, here,
                                here is the text. </p>
                        </div>
                    </div>

                </Carousel.Item>
                <Carousel.Item>
                    <div className="row align-items-center">
                        <div className="col-4">
                            <img
                                className="d-block w-100 photo"
                                src="university2.jpg"
                                alt="First slide"
                            />
                        </div>
                        <div className="col-8 py-2">
                            <p> Text text text here. Text here, here, here, here is the text. Text here, here, here,
                                here is the text. </p>
                            <p> Text text text here. Text here, here, here, here is the text. Text here, here, here,
                                here is the text. </p>
                        </div>
                    </div>
                </Carousel.Item>
                <Carousel.Item>
                    <div className="row align-items-center">
                        <div className="col-4">
                            <img
                                className="d-block w-100 photo"
                                src="university1.jpg"
                                alt="First slide"
                            />
                        </div>
                        <div className="col-8 py-2">
                            <p> Text text text here. Text here, here, here, here is the text. Text here, here, here,
                                here is the text. </p>
                            <p> Text text text here. Text here, here, here, here is the text. Text here, here, here,
                                here is the text. </p>
                        </div>
                    </div>
                </Carousel.Item>
            </Carousel>
        </div>
    );
}

export default HomeComponent
