import './MyCourses.css'
import {useTranslation} from "react-i18next";
import {useEffect, useState} from "react";
import API from "../../API/API";
import EditIcon from '@mui/icons-material/Edit';
import BootstrapTable from 'react-bootstrap-table-next';
import VisibilityIcon from '@mui/icons-material/Visibility';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ToolkitProvider, {Search} from 'react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit';
import cellEditFactory from 'react-bootstrap-table2-editor';
import { Type } from 'react-bootstrap-table2-editor';
import { type } from '@testing-library/user-event/dist/type';
import { textAlign } from '@mui/system';

function MyCourses() {

    const {t} = useTranslation();

    const [myCourses , setMyCourses] = useState([]);
    useEffect(()=>{
      
        API.getCoursesForPerson(sessionStorage.getItem("id"), sessionStorage.getItem("role")).then(result=>setMyCourses(result['body']));
    },[])

    const products = myCourses;
    let columns = [
        { dataField: 'id', text: 'ID', sort: false },
        { dataField: 'name', text: t('NAME'), sort: true },
        { dataField: 'teacher_id', text: t('TEACHER'), sort: true },
        { dataField: 'year', text: t('YEAR'), sort: true },
        { dataField: 'semester', text: t('SEMESTER'), sort: true }, { dataField: "View", 
        text: "View",
        sort: false,
        formatter: viewFormatter,
        headerAttrs: { width: 50 },
        attrs: { width: 50, className: "viewButton" } },
    ];
    function editFormatter(cell, row, rowIndex, formatExtraData) { 
        return ( 
              < div onClick={()=>{console.log(products[rowIndex]['id'])}}
                    className = "editButton">
                   
           < EditIcon
                
             style={{fontSize:20}}
             
            /> 
         </div> 
    ); } 
    function deleteFormatter(cell, row, rowIndex, formatExtraData) { 
        return ( 
              < div onClick={()=>{console.log(products[rowIndex]['id'])}}
                    className = "editButton">
                   
           < DeleteForeverIcon
                
             style={{fontSize:20}}
             
            /> 
         </div> 
    ); } 
    function viewFormatter(cell, row, rowIndex, formatExtraData) { 
        return ( 
              < div onClick={()=>{console.log(products[rowIndex]['id'])}}
                    className = "editButton">
                   
           < VisibilityIcon
                
             style={{fontSize:20}}
             
            /> 
         </div> 
    ); } 

    if (sessionStorage.getItem('role') === 'teacher')
     columns = [
        { dataField: 'name', text: t('NAME'), sort: true },
        { dataField: 'year', text: t('YEAR'), sort: true },
        { dataField: 'semester', text: t('SEMESTER'), sort: true },
       
            
            { dataField: "View", 
            text: "View",
            sort: false,
            formatter: viewFormatter,
            headerAttrs: { width: 50 },
            attrs: { width: 50, className: "viewButton" } },
            { dataField: "edit", 
            text: "Edit",
            sort: false,
            formatter: editFormatter,
            headerAttrs: { width: 50 },
            attrs: { width: 50, className: "editButton" } },
            { dataField: "Delete", 
            text: "Delete",
            sort: false,
            formatter: deleteFormatter,
            headerAttrs: { width: 75, textAlign: 'center' },
            attrs: { width: 50, className: "deleteButton" } },
        ];
   
  
    const defaultSorted = [
        { dataField: 'name', order: 'desc'}
    ];

    const { SearchBar, ClearSearchButton } = Search;

    return(
        <div className="MyCourses">
            <h5>Moje kursy</h5>

            <ToolkitProvider
                bootstrap4 keyField='id'
                data={products}
                columns={columns}
                search
            >
                {
                    props => (
                        <div>
                            <SearchBar srText={t("SEARCH_TABLE")} placeholder={t("SEARCH")} {...props.searchProps} />
                            <ClearSearchButton text={t("CLEAR")} {...props.searchProps} />
                            <hr/>
                            <BootstrapTable
                                defaultSorted={defaultSorted}
                                pagination={paginationFactory({ sizePerPage: 10 })}
                                {...props.baseProps}
                                
                            />
                        </div>
                    )
                }
            </ToolkitProvider>
        </div>
    );
}
export default MyCourses;
