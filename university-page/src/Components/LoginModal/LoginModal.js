import React, { useState,useEffect, useContext } from 'react'
import {Modal,Form,Button,Row,Col} from 'react-bootstrap'

import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';
import './LoginModal.css'

import { useTranslation } from 'react-i18next';
import "../../translations/i18n";

import PersonIcon from '@mui/icons-material/Person';
import PersonAddAlt1Icon from '@mui/icons-material/PersonAddAlt1';
import AuthContext from '../AuthContext/AuthContext';
import API from '../../API/API';

const LoginModal = (props) => {

    const {t} = useTranslation();
    const [value, setValue] = React.useState('one');
    const handleChange = (event, newValue) => {setValue(newValue);}
    const [registered,setRegistered] = useState(false);


    const [name,setName] = useState("");
    const [surname,setSurname] = useState("");
    const [password,setPassword] = useState("");
    const [email,setEmail] = useState("");
    const [phone,setPhone] = useState("");
    const [role,setRole] = useState("student");
    const [degree,setDegree] = useState("");
    const [isValid,setValid] = useState(false);

    const [nameType,setNameType] = useState(false);
    const [surnameType,setSurnameType] = useState(false);
    const [passwordType,setPasswordType] = useState(false);
    const [emailType,setEmailType] = useState(false);
    const [phoneType,setPhoneType] = useState(false);

    const [nameStyle,setNameStyle] = useState("mb-3");
    const [surnameStyle,setSurnameStyle] = useState("mb-3");
    const [passwordStyle,setPasswordStyle] = useState("mb-3");
    const [emailStyle,setEmailStyle] = useState("mb-3");
    const [phoneStyle,setPhoneStyle] = useState("mb-3");
    const [error,setError] = useState(false);



    const ctx = useContext(AuthContext);

    const login = ()=>{
        const loginData = {email: email, password: password};
        API.login(loginData).then(r=>{
            console.log(r)
            if (r === "WRONG EMAIL OR PASSWORD"){
                setEmailStyle('mb-3 wrong')
                setPassword('mb-3 wrong')
                setError(true);
            }
            else{
                props.onHide();
                sessionStorage.clear();
                ctx.logInHandler(r['role']);
                sessionStorage.setItem("id", r["id"]);
                window.location.reload(false);

            }
        })
      }

        const register = ()=>{
          const registerData = {
            name: name,
            lastname: surname,
            email: email, 
            password: password,
            universityID: ("ICSD"+Math.round(Math.random() * 1000000000)),
            phoneNumber: '+30'+phone,
            role:role,
            degree:degree,
            note: ""
          };
          API.createPerson(registerData).then(r=>{
              if (r === "Email taken"){
                  setEmailStyle('mb-3 wrong')
                  setError(true);
              }
              else{
                  setRegistered(true)
              }
          })


    }


    useEffect(() => {
        if (nameType){
      const delayDebounceFn = setTimeout(() => {
        const nameValid = /^[A-Z]{1}[a-z]+$/.test(name);
      
        if (!nameValid){
            setNameStyle("mb-3 wrong")
        }
        else setNameStyle("mb-3")
      }, 1000)
      return () => clearTimeout(delayDebounceFn)
    }}, [name])

    useEffect(() => {
        if (surnameType){
        const delayDebounceFn = setTimeout(() => {
            
        const surnameValid = /^[A-Z]{1}[a-z]+$/.test(surname);
            
        if (!surnameValid){
            setSurnameStyle("mb-3 wrong")
        }
        else setSurnameStyle("mb-3")
        }, 1000)
        return () => clearTimeout(delayDebounceFn)
      }}, [surname])

      useEffect(() => {
        if (emailType){
        const delayDebounceFn = setTimeout(() => {
         const emailValid = /^[a-zA-Z0-9]+@(aegean)\.(gr)$/.test(email)
            
        if (!emailValid){
            setEmailStyle("mb-3 wrong")
        }
        else setEmailStyle("mb-3")

        }, 1000)
        return () => clearTimeout(delayDebounceFn)
      }}, [email])


      useEffect(() => {
        if (phoneType){
        const delayDebounceFn = setTimeout(() => {
        
            const phoneValid = /^69([0-9]{8})+$/.test(phone);
            
            if (!phoneValid){
                setPhoneStyle("mb-3 wrong")
            }
            else setPhoneStyle("mb-3")

        }, 1000)
        return () => clearTimeout(delayDebounceFn)
      }}, [phone])

      useEffect(() => {
        if (passwordType){
        const delayDebounceFn = setTimeout(() => {
            const passValid = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$*]).{8,}$/.test(password)
            if (!passValid){
                setPasswordStyle("mb-3 wrong")
            }

            else setPasswordStyle("mb-3")


        }, 1000)
        return () => clearTimeout(delayDebounceFn)
      }}, [password])


      useEffect(() => {
        const nameValid = /^[A-Z]{1}[a-z]+$/i.test(name);
        const surnameValid = /^[A-Z]{1}[a-z]+$/i.test(surname);
        const phoneValid = /^69([0-9]{8})+$/.test(phone);
        const emailValid = /^[a-zA-Z0-9]+@(aegean)\.(gr)$/.test(email)
        const passValid = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$*]).{8,}$/.test(password)
        setValid(nameValid&&surnameValid&&phoneValid&&emailValid&&passValid);
        if (name && surname && email && password && phone){
        const delayDebounceFn = setTimeout(() => {
         const nameValid = /^[A-Z]{1}[a-z]+$/i.test(name);
         const surnameValid = /^[A-Z]{1}[a-z]+$/i.test(surname);
         const phoneValid = /^69([0-9]{8})+$/.test(phone);
         const emailValid = /^[a-zA-Z0-9]+@(aegean)\.(gr)$/.test(email)
         const passValid = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$*]).{8,}$/.test(password)
          console.log(role,degree)
         setValid(nameValid&&surnameValid&&phoneValid&&emailValid&&passValid && ((role === "teacher" && degree !== "") || role==="student" ));

        }, 1000)
        return () => clearTimeout(delayDebounceFn)}
    
      }, [email,password,name,surname,phone,role,degree])
    


      const clearAllStates = () => {
        setName("");
        setSurname("");
        setPhone("");
        setPassword("");
        setEmail("");
        setNameType(false);
        setSurnameType(false);
        setPhoneType(false);
        setPasswordType(false);
        setEmailType(false);

        setNameStyle("mb-3");
        setSurnameStyle("mb-3");
        setPhoneStyle("mb-3");
        setPasswordStyle("mb-3");
        setEmailStyle("mb-3");

      }


      const ifNotAfterRegister = <>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <Tabs
            value={value}
            onChange={handleChange}
            textColor="primary"
            indicatorColor="primary"
            aria-label="secondary tabs"
            centered
            variant="fullWidth"
          >
            <Tab icon={<PersonIcon/>} onClick={clearAllStates} iconPosition="start" value="one" label={t("LOGIN")}/>
            <Tab icon={<PersonAddAlt1Icon/>} onClick={clearAllStates} iconPosition="start" value="two" label={t("REGISTER")}/>
          </Tabs>
        </Box>

        {value === "one" && <div>
            <Modal.Header>
              <Modal.Title>{t("LOGIN")}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <Form.Group  controlId="exampleForm.ControlInput1">
                  <Form.Label> Email address</Form.Label>
                  <Form.Control  onChange= {(e)=>{setEmail(e.target.value); setEmailType(true); }} className={emailStyle}  type="email" placeholder="name@example.com" autoFocus/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="exampleForm.ControlInput2">
                  <Form.Label>Password</Form.Label>
                  <Form.Control  className={passwordStyle} onChange= {(e)=>{setPassword(e.target.value);setPasswordType(true);}} type="password" placeholder="password"/>
                </Form.Group>
              </Form>
              {error && <p>Wrong email or password</p>}
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={props.onHide}>
                  {t("CLOSE")}
              </Button>
              <Button variant="primary" onClick={login}>
                  {t("LOGIN")}
              </Button>
            </Modal.Footer>
        </div>}

        {value === "two" && <div>
            <Modal.Header>
              <Modal.Title>{t("REGISTER")}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                  <Form.Label>Name</Form.Label>
                  <Form.Control className={nameStyle} onChange= {(e)=>{setName(e.target.value);setNameType(true);}} type="text" placeholder="Name" autoFocus/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="exampleForm.ControlInput2">
                  <Form.Label>Surname</Form.Label>
                  <Form.Control className={surnameStyle} onChange= {(e)=>{setSurname(e.target.value);setSurnameType(true);}} type="text" placeholder="Surname"/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="exampleForm.ControlInput3">
                  <Form.Label>Email address</Form.Label>
                  <Form.Control  className={emailStyle} onChange= {(e)=>{setEmail(e.target.value);setEmailType(true);}} type="email" placeholder="name@example.com"/>
                  {error && <p>Email already taken </p>}
                </Form.Group>
                <Form.Group className="mb-0" controlId="exampleForm.ControlInput4">
                  <Form.Label>Phone number</Form.Label>
                    <Form.Group className='mb-0' as={Row}>
                    <Form.Label column sm='1'>+30</Form.Label>
                    <Col sm="11">
                    <Form.Control className={phoneStyle} onChange= {(e)=>{setPhone(e.target.value);setPhoneType(true);}}  type="phone" placeholder="69xxxxxxxx"/>
                    </Col>
                    </Form.Group>
                </Form.Group>
                <Form.Group className="mb-3" controlId="exampleForm.ControlInput5">
                  <Form.Label>Password</Form.Label>
                  <Form.Control  className={passwordStyle} onChange= {(e)=>{setPassword(e.target.value);setPasswordType(true);}}  type="password" placeholder="password"/>
                  <Form.Text className="text-muted">
                      Password should have jakies tam znaki
                  </Form.Text>
                </Form.Group>
                <Form.Group className="mb-3" controlId="exampleForm.ControlSelect">
                  <Form.Label>Choose role</Form.Label>
                  <Form.Select onChange={(e)=>{setRole(e.target.value); }} aria-label="Default select example">
                      <option value="student">Student</option>
                      <option value="teacher">Teacher</option>
                  </Form.Select>
                </Form.Group>
                {role === 'teacher' && <Form.Group className="mb-3" controlId="exampleForm.ControlSelect">
                  <Form.Label>Choose degree</Form.Label>
                  <Form.Select onChange={(e)=>{setDegree(e.target.value); }} aria-label="Default select example">
                      <option selected disabled> Choose Degree</option>
                      <option value="Assistant">Assistant</option>
                      <option value="Deputy">Deputy</option>
                      <option value="Teacher">Teacher</option>
                  </Form.Select>
                </Form.Group>}
              </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={props.onHide}>
                    {t("CLOSE")}
                </Button>
                {isValid && <Button variant="primary" onClick={register}>
                    {t("REGISTER")}
                </Button>}
            </Modal.Footer>
        </div>}
      </>


    return <>
    <Modal show={props.show} onHide={props.onHide}>
        {!registered && ifNotAfterRegister}  
        {registered && <><p> Registered sucessfully, Wait For the admin acceptance</p></>}
      </Modal>
    </>

}

export default LoginModal;
