import './Navbar.css';
import React, {useContext, useEffect, useState} from "react";
import AuthContext from "../AuthContext/AuthContext";
import {Container, Nav, NavDropdown, Navbar as Navi, Button} from "react-bootstrap";
import { useTranslation } from 'react-i18next';
import "../../translations/i18n";
import i18n from "i18next";
import {Link} from 'react-router-dom'
import ReactCountryFlag from "react-country-flag"
import LanguageIcon from '@mui/icons-material/Language';
import LoginModal from '../LoginModal/LoginModal';


function Navbar() {

    const {t} = useTranslation();
    
    const ctx = useContext(AuthContext);
    const [showLogModal, setShow] = useState(false);

    const openCloseModal = (state) => {
        setShow(state);
    }

    return(

        <Navi collapseOnSelect expand="lg" className="my-navbar">
            <Container>
                <Navi.Brand as={Link} to="/home">
                    <img src="logo.png" height="60"/>
                    <span id="title">{t('TITLE')}</span>
                </Navi.Brand>
                <Navi.Toggle aria-controls="responsive-navbar-nav" />
                <Navi.Collapse id="responsive-navbar-nav">
                    <Nav className="ms-auto">
                        {showLogModal && <LoginModal show = {showLogModal} onHide = {()=>{openCloseModal(false); window.location.reload();}}/>}
                        <Link className='nav-link' to="/courses">{t('COURSES')}</Link>
                        {sessionStorage.getItem("isLoggedin") === "true" && (sessionStorage.getItem("role")  === "student" ||sessionStorage.getItem("role")  === "teacher" ) &&<Link className='nav-link' to="/mycourses">{t('MY_COURSES')}</Link>}
                        {sessionStorage.getItem("isLoggedin") === "true" && sessionStorage.getItem("role")  === "student" &&<Link className='nav-link' to="/mygrades">{t('MY_GRADES')}</Link>}
                        {sessionStorage.getItem("isLoggedin") === "true" && (sessionStorage.getItem("role")  === "student" ||sessionStorage.getItem("role")  === "teacher" ) &&<Link className='nav-link' to="/studentstatistics">{t('STATS')}</Link>}
                        {sessionStorage.getItem("isLoggedin") === "true" && (sessionStorage.getItem("role")  === "student" ||sessionStorage.getItem("role")  === "teacher" ) &&<Link className='nav-link' to="/myaccount">{t('MY_ACCOUNT')}</Link>}
                        {(sessionStorage.getItem("isLoggedin") !== "true") && <Button onClick={()=>openCloseModal(true)} className="btn">{t('LOGIN')}</Button>}
                        {(sessionStorage.getItem("isLoggedin") === "true") && <Button href="/home" onClick={()=>ctx.logOutHandler()} className="btn">{t('LOGOUT')}</Button>}
                        <NavDropdown  title={<LanguageIcon></LanguageIcon>}>
                            <NavDropdown.Item onClick={()=> {
                         sessionStorage.setItem('lang',"en")
                        i18n.changeLanguage(sessionStorage.getItem("lang"))
                        }}> <ReactCountryFlag countryCode="GB" svg /> {t('ENG')}</NavDropdown.Item>
                            <NavDropdown.Item onClick={()=> {
                           sessionStorage.setItem('lang',"pl")
                           i18n.changeLanguage(sessionStorage.getItem("lang"))
                        }}><ReactCountryFlag countryCode="PL" svg /> {t('PL')}</NavDropdown.Item>
                            <NavDropdown.Item onClick={()=> {
                            sessionStorage.setItem('lang',"gr")
                            i18n.changeLanguage(sessionStorage.getItem("lang"))
                        }}><ReactCountryFlag countryCode="GR" svg /> {t('GR')}</NavDropdown.Item>
                        </NavDropdown>
                        
                    </Nav>
                </Navi.Collapse>
            </Container>
        </Navi>
    );
}
export default Navbar;
