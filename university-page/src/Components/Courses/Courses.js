import './Courses.css'
import API from "../../API/API";
import {useEffect, useState} from "react";
import { useTranslation } from 'react-i18next';
import "../../translations/i18n";
import VisibilityIcon from '@mui/icons-material/Visibility';
import BootstrapTable from 'react-bootstrap-table-next';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ToolkitProvider, {Search} from 'react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit';


function Courses() {

    const {t} = useTranslation();

    const [allCourses , setAllCourses] = useState([]);
    useEffect(()=>{
        API.getAllCourses().then(result=>setAllCourses(result['body']));
    },[])
    function viewFormatter(cell, row, rowIndex, formatExtraData) { 
        return ( 
              < div onClick={()=>{console.log(products[rowIndex]['id'])}}
                    className = "editButton">
                   
           < VisibilityIcon
                
             style={{fontSize:20}}
             
            /> 
         </div> 
    ); } 

    const products = allCourses;
    const columns = [
        { dataField: 'name', text: t('NAME'), sort: true },
        { dataField: 'teacher_id', text: t('TEACHER'), sort: true },
        { dataField: 'year', text: t('YEAR'), sort: true },
        { dataField: 'semester', text: t('SEMESTER'), sort: true },
        { dataField: "View", 
        text: "View",
        sort: false,
        formatter: viewFormatter,
        headerAttrs: { width: 50 },
        attrs: { width: 50, className: "viewButton" } },
    ];

    const defaultSorted = [
        { dataField: 'name', order: 'desc'}
    ];

    const { SearchBar, ClearSearchButton } = Search;

    return(
        <div className="Courses">
            <h5>{t("COURSES")}</h5>

            <ToolkitProvider
                bootstrap4 keyField='id'
                data={products}
                columns={columns}
                search
            >
                {
                    props => (
                        <div>
                            <SearchBar srText={t("SEARCH_TABLE")} placeholder={t("SEARCH")} {...props.searchProps} />
                            <ClearSearchButton text={t("CLEAR")} {...props.searchProps} />
                            <hr />
                            <BootstrapTable
                                defaultSorted={defaultSorted}
                                pagination={paginationFactory({ sizePerPage: 10 })}
                                {...props.baseProps}
                            />
                        </div>
                    )
                }
            </ToolkitProvider>
        </div>
    );
}
export default Courses;

//<BootstrapTable bootstrap4 keyField='id' data={products} columns={columns} defaultSorted={defaultSorted} pagination={paginationFactory({ sizePerPage: 10 })}/>
