import React from 'react';
import { useState } from 'react';


const AuthContext = React.createContext({
    isLoggedin: false,
    logInHandler: (role) => { },
    logOutHandler: () => { },
})

export const AuthContextProvider = (props) => {
    const [logggedIN, setLogIN] = useState(false);
    const [role, setRole] = useState("");

    const logIn = (role) => {
        setLogIN(true);
        sessionStorage.setItem("isLoggedin", true)
        sessionStorage.setItem("role", role)
        window.setTimeout(() => {   sessionStorage.removeItem("isLoggedin");
        sessionStorage.removeItem("role"); window.location.reload() }, 3600000 )
    }
    const logOut = () => {
        setRole("")
        sessionStorage.removeItem("isLoggedin");
        sessionStorage.removeItem("role");
        sessionStorage.removeItem("id");
        setLogIN(false);
        window.location.reload();
    }

    return <AuthContext.Provider value={{ isLoggedin: logggedIN, logInHandler: logIn, logOutHandler: logOut, role: role }}>{props.children}</AuthContext.Provider>
}

export default AuthContext;
