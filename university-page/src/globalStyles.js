import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
    background: url("tlo.png");
    background-size: cover;
    font-family: Montserrat;
  }
`;

export default GlobalStyle;
