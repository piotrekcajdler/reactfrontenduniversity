import React from 'react';
import './App.css';
import  API from './API/API'
import HomeComponent from './Components/Home/HomeComponent';
import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import Navbar from "./Components/Navbar/Navbar";
import Courses from "./Components/Courses/Courses";
import MyCourses from "./Components/MyCourses/MyCourses";
import GlobalStyle from "./globalStyles";

function App() {
  return (

      <Router>
        <GlobalStyle></GlobalStyle>
        <Navbar></Navbar>
          <Routes>
              <Route exact path='/home' element={<HomeComponent/>}/>
              <Route exact path='/courses' element={<Courses/>}/>
              <Route exact path='/mycourses' element={<MyCourses/>}/>
              <Route exact path='/' element={<HomeComponent/>}/>
              <Route path='*' element={<HomeComponent/>}/>
          </Routes>
        
      </Router>
  );
}

export default App;
