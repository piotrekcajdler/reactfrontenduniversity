import axios from "axios";

const backendAddress = "http://localhost:8080"
axios.defaults.withCredentials = true;
const API = {
    getAllPeople: async function () {
        return await axios.get(backendAddress+'/getAllPeople.php').then((res)=>res.data)
    },
    getAllGradesFromTheCourse: async function(id) {
        return await axios.get(backendAddress+'/getAllGradesForCourse.php?cid='+id).then((res)=>res.data)
    },
    getAllGradesForTheStudent: async function(id) {
        return await axios.get(backendAddress+'/getGradesForStudent.php?sid='+id).then((res)=>res.data)
    },
    getFreeTeachers: async function () {
        return await axios.get(backendAddress+'/getFreeTeachers.php').then((res)=>res.data)
    },
    getAllGradesForGivenStudentAndCourse: async function (student_id,course_id) {
        return await axios.get(backendAddress+'/getGradesForStudentForCourse.php?sid='+ student_id + '&cid=' + course_id).then((res)=>res.data)
    },
    getFinalGradeForStudentAndCourse: async function (student_id,course_id) {
        return await axios.get(backendAddress+'/getFinalGradeForStudent.php?sid='+ student_id + '&cid=' + course_id).then((res)=>res.data)
    },
    getAllGrades: async function () {
        return await axios.get(backendAddress+'/getAllGrades.php').then((res)=>res.data)
    },
    getAllStudentsFromTheCourse: async function(id) {
        return await axios.get(backendAddress+'/getAllStudentFromCourse.php?cid='+id).then((res)=>res.data)
    },
    getPersonByID: async function(id) {
        return await axios.get(backendAddress+'/getPersonByID.php?id='+id).then((res)=>res.data)
    },
    getAllRelatedCourses: async function(id) {
        return await axios.get(backendAddress+'/getRelatedCourses.php?cid='+id).then((res)=>res.data)
    },
    getProfessorFromCourse: async function(id) {
        return await axios.get(backendAddress+'/getProfessorFromCourse.php?cid='+id).then((res)=>res.data)
    },
    getCourseByID: async function(id) {
        return await axios.get(backendAddress+'/getCourseByID.php?id='+id).then((res)=>res.data)
    },
    getGradeByID: async function(id) {
        return await axios.get(backendAddress+'/getGradeByID.php?id='+id).then((res)=>res.data)
    },
    getAllCourses: async function () {
        return await axios.get(backendAddress+'/getAllCourses.php').then((res)=>res.data)
    },
    updatePersonByID: async function (person) {
        return await axios.patch(backendAddress+'/updatePeopleByID.php',person).then((res)=>res.data)
    },
    updateGradeByID: async function (grade) {
        return await axios.patch(backendAddress+'/updateGrade.php',grade).then((res)=>res.data)
    },
    updateCourseByID: async function (course) {
        return await axios.patch(backendAddress+'/updateCourse.php',course).then((res)=>res.data)
    },
    deleteUserByID: async function (id) {
        return await axios.delete(backendAddress+'/deleteUserByID.php?id='+id).then((res)=>res.data)
    },
    deleteCourseByID: async function (id) {
        return await axios.delete(backendAddress+'/deleteCourseByID.php?id='+id).then((res)=>res.data)
    },
    deleteRelatedCourse: async function (ids) {
        return await axios.post(backendAddress+'/removeRelatedCourse.php',ids).then((res)=>res.data)
    },
    deleteStudentFromCourse: async function (ids) {
        return await axios.post(backendAddress+'/removeStudentFromCourse.php',ids).then((res)=>res.data)
    },
    createPerson: async function (person) {
        return await axios.post(backendAddress+'/createPerson.php',person).then((res)=>res.data)
    },
    unBindTeacherFromCourse: async function (courseid) {
        return await axios.post(backendAddress+'/unbindTeacherFromCourseByID.php?id='+courseid).then((res)=>res.data)
    },
    bindTeacherToCourse: async function (courseid,teacher_id) {
        return await axios.post(backendAddress+'/bindTeacherToCourse.php?cid='+courseid+'&tid='+teacher_id).then((res)=>res.data)
    },
    createCourse: async function (course) {
        return await axios.post(backendAddress+'/createCourse.php',course).then((res)=>res.data)
    },
    addStudentToCourse: async function (details) {
        return await axios.post(backendAddress+'/addStudentToCourse.php',details).then((res)=>res.data)
    },
    addRelatedCourse: async function (details) {
        return await axios.post(backendAddress+'/addRelatedCourse.php',details).then((res)=>res.data)
    },
    createGrade: async function (grade,isFin) {
        return await axios.post(backendAddress+'/createGrade.php?isfin='+isFin,grade).then((res)=>res.data)
    },
    delOrSubGrade: async function (operation,id) {
        return await axios.post(backendAddress+'/delSubGrade.php?id='+id+"&op="+operation).then((res)=>res.data)
    },
    login: async function (logdata) {
        return await axios.post(backendAddress+'/login.php',logdata).then((res)=>res.data)
    },
    getCoursesForPerson: async function(id,role){
        return await axios.get(backendAddress+'/getCoursesForPerson.php?id='+id+'&role='+role).then((res)=>res.data)
    },

}

export default API;
